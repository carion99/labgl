let connection = require('../config/connection')

class Cursus {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_cursus'
	}
	/**
	 * Constante contenant le dateDebut de la table actuelle
	 */
	static TABLE() {
		return 'cursus'
	}


	get id () {
		return this.row.id
	}
	get codeCursus () {
		return this.row.code_cursus
    }
    
    get codeEtudiant () {
		return this.row.code_etudiant
	}

	get dateDebut () {
		return this.row.date_debut
	}
	set dateDebut (dateDebut) {
		let codeCursus = this.codeCursus;
		Cursus.replaceByOneField(codeCursus, 'date_debut', dateDebut, (msg) => {
			console.log(msg);
		})
	}

	get dateFin () {
		return this.row.date_fin
	}
	set dateFin (dateFin) {
		let codeCursus = this.codeCursus;
		Cursus.replaceByOneField(codeCursus, 'date_fin', dateFin, (msg) => {
			console.log(msg);
		})
    }
    
    get diplome () {
		return this.row.diplome
	}
	set diplome (diplome) {
		let codeCursus = this.codeCursus;
		Cursus.replaceByOneField(codeCursus, 'diplome', diplome, (msg) => {
			console.log(msg);
		})
	}



	static create(content, cb) {
		let ID 		= Cursus.ID()
		let TABLE 	= Cursus.TABLE()
        let codeCursus 	= content.codeCursus
        let codeEtudiant= content.codeEtudiant
		let dateDebut 		= content.dateDebut
		let dateFin  	= content.dateFin
        let diplome  	= content.diplome
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_etudiant = ?, date_debut = ?, date_fin = ?, diplome = ?'  
		connection.query(sql, [codeCursus, codeEtudiant, dateDebut, dateFin, diplome],
			(err, result) => {
				if (err) throw err
				let msg = "Cursus bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Cursus.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Cursus(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Cursus.TABLE()
		let ID = Cursus.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Cursus.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Cursus(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeCursus = row.codeCursus
		let TABLE = Cursus.TABLE()
		let ID = Cursus.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeCursus], (err, result) => {
			if (err) throw err
			let msg = "Cursus supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Cursus