let connection = require('../config/connection')

class InfoPersonnel {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_info_perso'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'info_personnel'
	}


	get id () {
		return this.row.id
	}
	get codeInfoPerso () {
		return this.row.code_info_perso
    }
    
    get codeEtudiant () {
		return this.row.code_etudiant
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.email
	}
	set email (email) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'email', email, (msg) => {
			console.log(msg)
		})
	}

	get age () {
		return this.row.age
	}
	set age (age) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'age', age, (msg) => {
			console.log(msg)
		})	
	}

	get phone1 () {
		return this.row.phone1
	}
	set phone1 (phone1) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'phone1', phone1, (msg) => {
			console.log(msg);
		})
    }

    get phone2 () {
		return this.row.phone2
	}
	set phone2 (phone3) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'phone2', phone2, (msg) => {
			console.log(msg);
		})
    }
    get phone3 () {
		return this.row.phone3
	}
	set phone3 (phone3) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'phone3', phone3, (msg) => {
			console.log(msg);
		})
    }

    get nbEnfant () {
		return this.row.nb_enfant
	}
	set nbEnfant (nbEnfant) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'nb_enfant', nbEnfant, (msg) => {
			console.log(msg);
		})
    }

	get statutMatri () {
		return this.row.date_register
	}
	set statutMatri (statutMatri) {
		let codeInfoPerso = this.codeInfoPerso;
		InfoPersonnel.replaceByOneField(codeInfoPerso, 'statutMatri', statutMatri, (msg) => {
			console.log(msg)
		})
	}

	static create(content, cb) {
		let ID 		= InfoPersonnel.ID()
		let TABLE 	= InfoPersonnel.TABLE()
		let codeInfoPerso 	= content.codeInfoPerso
		let nom 		= content.nom
		let prenom  	= content.prenom
		let email	= content.email
		let age 	= content.age
        let sexe  	= content.sexe
        let statutMatri= content.statutMatri
        let phone1  = content.phone1
        let phone2  = content.phone2
        let phone3  = content.phone3
        let nbEnfant  = content.nbEnfant
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_etudiant = ?, nom = ?, prenom = ?, email = ?,' 
		+'age = ?, sexe = ?, statut_matri = ?, nb_enfant = ?, phone1 = ?, phone2 = ?, phone3 = ?'
		connection.query(sql, [codeInfoPerso, codeEtudiant, nom, prenom, email, age, sexe, statutMatri, nbEnfant, phone1, phone2, phone3],
			(err, result) => {
				if (err) throw err
				let msg = "InfoPersonnel bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = InfoPersonnel.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new InfoPersonnel(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = InfoPersonnel.TABLE()
		let ID = InfoPersonnel.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = InfoPersonnel.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new InfoPersonnel(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeInfoPerso = row.codeInfoPerso
		let TABLE = InfoPersonnel.TABLE()
		let ID = InfoPersonnel.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeInfoPerso], (err, result) => {
			if (err) throw err
			let msg = "InfoPersonnel supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = InfoPersonnel