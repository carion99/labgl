let connection = require('../config/connection')

class User {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_user'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'user'
	}


	get id () {
		return this.row.id
	}
	get codeUser () {
		return this.row.code_user
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get username () {
		return this.row.username
	}
	set username (username) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'username', username, (msg) => {
			console.log(msg)
		})
	}

	get password () {
		return this.row.password
	}
	set password (password) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'password', password, (msg) => {
			console.log(msg)
		})	
	}

	get status () {
		return this.row.status
	}
	set status (status) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'status', status, (msg) => {
			console.log(msg);
		})
    }

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeUser = this.codeUser;
		User.replaceByOneField(codeUser, 'dateRegister', dateRegister, (msg) => {
			console.log(msg)
		})
	}

	static create(content, cb) {
		let ID 		= User.ID()
		let TABLE 	= User.TABLE()
		let codeUser 	= content.codeUser
		let nom 		= content.nom
		let prenom  	= content.prenom
		let username	= content.username
		let password 	= content.password
        let status  	= content.status
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, username = ?,' 
		+'password = ?, status = ?, date_register = ?'
		connection.query(sql, [codeUser, nom, prenom, username, password, status, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "User bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = User.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new User(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = User.TABLE()
		let ID = User.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = User.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new User(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeUser = row.codeUser
		let TABLE = User.TABLE()
		let ID = User.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeUser], (err, result) => {
			if (err) throw err
			let msg = "User supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = User