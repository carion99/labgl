//let connection = require('../config/connection')

class Root {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_root'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'root'
	}


	get id () {
		return this.row.id
	}
	get codeRoot () {
		return this.row.code_root
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.Rootname
	}
	set email (email) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'email', email, (msg) => {
			console.log(msg)
		})
	}

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
	}

	static genCodeRoot() {
		let min = 0
		let max = 9
		let text= "root-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= Root.ID()
		let TABLE 	= Root.TABLE()
		let codeRoot 	= content.codeRoot
		let nom 		= content.nom
		let prenom  	= content.prenom
		let email  	    = content.email
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, email = ?,' 
		+' date_register = ?'
		connection.query(sql, [codeRoot, nom, prenom, email, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Root bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Root.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Root(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Root.TABLE()
		let ID = Root.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Root.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Root(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeRoot = row.codeRoot
		let TABLE = Root.TABLE()
		let ID = Root.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeRoot], (err, result) => {
			if (err) throw err
			let msg = "Root supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Root