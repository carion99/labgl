let connection = require('../config/connection')

class Divers {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_divers'
	}
	/**
	 * Constante contenant le passion de la table actuelle
	 */
	static TABLE() {
		return 'divers'
	}


	get id () {
		return this.row.id
	}
	get codeDivers () {
		return this.row.code_Divers
	}

	get passion () {
		return this.row.passion
	}
	set passion (passion) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'passion', passion, (msg) => {
			console.log(msg);
		})
	}

	get hobbie () {
		return this.row.hobbie
	}
	set hobbie (hobbie) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'hobbie', hobbie, (msg) => {
			console.log(msg);
		})
	}

	get association () {
		return this.row.association
	}
	set association (association) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'association', association, (msg) => {
			console.log(msg)
		})
	}

	get password () {
		return this.row.password
	}
	set password (password) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'password', password, (msg) => {
			console.log(msg)
		})	
	}

	get description () {
		return this.row.description
	}
	set description (description) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'description', description, (msg) => {
			console.log(msg);
		})
    }

	get lv1 () {
		return this.row.lv1
	}
	set lv1 (lv1) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'lv1', lv1, (msg) => {
			console.log(msg)
		})
    }
    
    get lv2 () {
		return this.row.lv1
	}
	set lv2 (lv1) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'lv2', lv2, (msg) => {
			console.log(msg)
		})
    }
    
    get lv3 () {
		return this.row.lv3
	}
	set lv3 (lv3) {
		let codeDivers = this.codeDivers;
		Divers.replaceByOneField(codeDivers, 'lv3', lv3, (msg) => {
			console.log(msg)
		})
	}

	static create(content, cb) {
		let ID 		= Divers.ID()
		let TABLE 	= Divers.TABLE()
        let codeDivers 	= content.codeDivers
        let codeEtudiant 	= content.codeEtudiant
		let passion 		= content.passion
		let hobbie  	= content.hobbie
		let association	= content.association
        let description  = content.description
        let lv1 = content.lv1
        let lv2 = content.lv2
        let lv3 = content.lv3
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_etudiant, passion = ?, hobbie = ?, association = ?,' 
		+'password = ?, description = ?, lv1 = ?, lv2 = ?, lv3 = ?'
		connection.query(sql, [codeDivers, codeEtudiant, passion, hobbie, association, description, lv1, lv2, lv3],
			(err, result) => {
				if (err) throw err
				let msg = "Divers bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Divers.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Divers(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Divers.TABLE()
		let ID = Divers.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Divers.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Divers(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeDivers = row.codeDivers
		let TABLE = Divers.TABLE()
		let ID = Divers.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeDivers], (err, result) => {
			if (err) throw err
			let msg = "Divers supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Divers