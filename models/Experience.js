let connection = require('../config/connection')

class Experience {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_experience'
	}
	/**
	 * Constante contenant le dateDebut de la table actuelle
	 */
	static TABLE() {
		return 'exprerience'
	}


	get id () {
		return this.row.id
	}
	get codeExperience () {
		return this.row.code_experience
    }
    
    get codeEtudiant () {
		return this.row.code_etudiant
	}

	get dateDebut () {
		return this.row.date_debut
	}
	set dateDebut (dateDebut) {
		let codeExperience = this.codeExperience;
		Experience.replaceByOneField(codeExperience, 'date_debut', dateDebut, (msg) => {
			console.log(msg);
		})
	}

	get dateFin () {
		return this.row.date_fin
	}
	set dateFin (dateFin) {
		let codeExperience = this.codeExperience;
		Experience.replaceByOneField(codeExperience, 'date_fin', dateFin, (msg) => {
			console.log(msg);
		})
    }
    
    get libelle () {
		return this.row.libelle
	}
	set libelle (libelle) {
		let codeExperience = this.codeExperience;
		Experience.replaceByOneField(codeExperience, 'libelle', libelle, (msg) => {
			console.log(msg);
		})
	}



	static create(content, cb) {
		let ID 		= Experience.ID()
		let TABLE 	= Experience.TABLE()
        let codeExperience 	= content.codeExperience
        let codeEtudiant= content.codeEtudiant
		let dateDebut 		= content.dateDebut
		let dateFin  	= content.dateFin
        let libelle  	= content.libelle
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_etudiant = ?, date_debut = ?, dateFin = ?, libelle = ?'  
		connection.query(sql, [codeExperience, codeEtudiant, dateDebut, dateFin, libelle],
			(err, result) => {
				if (err) throw err
				let msg = "Experience bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Experience.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Experience(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Experience.TABLE()
		let ID = Experience.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Experience.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Experience(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeExperience = row.codeExperience
		let TABLE = Experience.TABLE()
		let ID = Experience.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeExperience], (err, result) => {
			if (err) throw err
			let msg = "Experience supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Experience