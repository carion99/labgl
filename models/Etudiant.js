let connection = require('../config/connection')

class Etudiant {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_etudiant'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'etudiant'
	}


	get id () {
		return this.row.id
	}
	get codeEtudiant () {
		return this.row.code_etudiant
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		letcodeEtudiant = this.codeEtudiant;
		Etudiant.replaceByOneField(codeEtudiant, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		letcodeEtudiant = this.codeEtudiant;
		Etudiant.replaceByOneField(codeEtudiant, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.email
	}
	set email (email) {
		letcodeEtudiant = this.codeEtudiant;
		Etudiant.replaceByOneField(codeEtudiant, 'email', email, (msg) => {
			console.log(msg);
		})
    }

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		letcodeEtudiant = this.codeEtudiant;
		Etudiant.replaceByOneField(codeEtudiant, 'dateRegister', dateRegister, (msg) => {
			console.log(msg)
		})
	}

	static create(content, cb) {
		let ID 		= Etudiant.ID()
		let TABLE 	= Etudiant.TABLE()
		let codeEtudiant 	= content.codeEtudiant
		let nom 		= content.nom
		let prenom  	= content.prenom
        let email  	= content.email
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, ' 
		+'email = ?, date_register = ?'
		connection.query(sql, [codeEtudiant, nom, prenom, email, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Etudiant bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Etudiant.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Etudiant(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Etudiant.TABLE()
		let ID = Etudiant.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Etudiant.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Etudiant(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		letcodeEtudiant = row.codeEtudiant
		let TABLE = Etudiant.TABLE()
		let ID = Etudiant.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeEtudiant], (err, result) => {
			if (err) throw err
			let msg = "Etudiant supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Etudiant