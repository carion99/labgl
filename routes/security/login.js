const express = require('express')
const router = express.Router()
const emailValidator = require('email-validator')
const html_specialchars = require('html-specialchars')
const mailer = require('nodemailer')

const Hashub = require('../../config/hashub')

const User = require('../../models/User')

/* GET home page. */

router.get('/', (req, res, next) => {
  let sess = req.session
    if (sess.codeUser) {
        if (sess.status === "root") {
          res.redirect('/compte/root')
        } 
        if (sess.status === "client") {
          res.redirect('/compte/client')
        } 
        if (sess.status === "presta") {
          res.redirect('/compte/presta')
        } 
    } else {
      res.render('security/login', { title: 'GeassCard: Connexion' })
    }
  
})

router.post('/', (req, res, next) => {
  let data = req.body

  console.log(data)

  let username = html_specialchars.escape(data.loginUsername)
  let password = html_specialchars.escape(data.loginPassword)

  let error = ""

  if (username && password) {
    if (emailValidator.validate(username)) {
      User.findByOneField('username', username, (futuser) => {
        if (futuser.length === 1) {
          user = futuser[0]
          if (user.status === "root" || user.status === "etudiant") {
            userpass = user.password
            let verif = Hashub.compare(password, userpass)
            if (verif) {
              let sess = req.session
              sess.codeUser 	= user.codeUser
              sess.username 	= user.username
              sess.nom      	= user.nom
              sess.prenom   	= user.prenom
              sess.status  	  = user.status
              sess.dateRegis  = user.dateRegister

              if (sess.status === "root") {
                res.redirect('/compte/root')
              }
              if (sess.status === "etudiant") {
                res.redirect('/compte/client')
              }

            } else {
              error = "Mot de passe erroné"
              let info = data
              res.render('security/login', 
                { 
                  title: 'GeassCard: Connexion',
                  info : info,
                  error: error
                })
            }
          } else {
            error = "Veuillez confirmez votre enregistrement d'abord, Ps: un email vous a été envoyé"
            let info = data
            res.render('security/login', 
              { 
                title: 'GeassCard: Connexion',
                info : info,
                error: error
              })
          }
        } else {
          error = "Utilisateur non existant"
          let info = data
          res.render('security/login', 
            { 
              title: 'GeassCard: Connexion',
              info : info,
              error: error
            })
        }
      })
    } else {
      error = "Format Email Incorrecte"
      let info = data
      res.render('security/login', 
        { 
          title: 'GeassCard: Connexion',
          info : info,
          error: error
        })
    }
  } else {
    error = "Tous les champs sont obligatoires"
    let info = data
    res.render('security/login', 
      { 
        title: 'GeassCard: Connexion',
        info : info,
        error: error
      })
  }
  
})

module.exports = router

